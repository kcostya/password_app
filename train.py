from lgbm_model.model import PasswordLGBM


def main():
    password_model = PasswordLGBM()
    password_model.fit(train_file="train.csv.zip", test_file="Xtest.csv.zip")
    password_model.save_vectorizer("vectorizer.pickle")
    password_model.save_model("lgbm_model")


if __name__ == "__main__":
    main()
