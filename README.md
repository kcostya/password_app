# Password App

Flask-ML application to predict given password frequency

[Heroku Demo](https://fierce-lake-31406.herokuapp.com/)

## Installation

First clone the repo locally:
~~~bash
$ git clone https://gitlab.com/production-ml/password_app
$ cd password_app
~~~

Install Pipenv and its dependencies:
~~~bash
$ pip install pipenv
~~~

Activate the virtual environment:
~~~bash
$ pipenv shell
$ pipenv install --dev
~~~

Use the following commands to deploy:
~~~bash
$ pipenv shell
$ pipenv install
~~~

Run the web application via:
~~~bash
$ python app.py
~~~

Deactivate the virtual environment:
~~~bash
$ exit
~~~

## Development

To commit changes, first run `pre-commit install`. If you have no pre-commit installed, you can do it following instructions at https://pre-commit.com
