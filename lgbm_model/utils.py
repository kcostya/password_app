import pickle
import re
from typing import Union

import numpy as np
import pandas as pd
from scipy.sparse import hstack
from sklearn.metrics import mean_squared_log_error


def rmsle_lgbm(y_true, y_pred):
    y_true, y_pred = np.exp(y_true), np.exp(y_pred)
    y_pred[y_pred < 0] = 0
    y_true[y_true < 0] = 0
    score = np.sqrt(mean_squared_log_error(y_true, y_pred))
    return "rmsle", score, False


def process_password(vectorizer, password: str):
    """Return sequence of tokens from password"""
    with open(vectorizer, "rb") as handle:
        vectorizer = pickle.load(handle)
    password = " ".join(re.findall(r"\S", str(password)))
    features = add_features(password)
    vec_password = vectorizer.transform([password])
    preds_data = hstack(
        [vec_password, features.drop(columns=["Password"], errors="ignore").values]
    )
    return preds_data


def add_features(data: Union[str, pd.DataFrame]) -> pd.DataFrame:
    if isinstance(data, str):
        data = pd.DataFrame({"Password": data}, index=[0])
    data["length"] = data["Password"].apply(len)
    data["n_capitals"] = data["Password"].str.findall(r"[A-Z]").apply(len)
    data["n_digits"] = data["Password"].str.findall(r"\d").apply(len)
    data["n_symbols"] = data["Password"].str.findall(r"[^aA-zZ|^0-9]").apply(len)

    data["caps_vs_length"] = data.apply(
        lambda row: float(row["n_capitals"]) / float(row["length"]), axis=1
    )
    data["digits_vs_length"] = data.apply(
        lambda row: float(row["n_digits"]) / float(row["length"]), axis=1
    )
    data["symbols_vs_length"] = data.apply(
        lambda row: float(row["n_symbols"]) / float(row["length"]), axis=1
    )

    data["n_unique_chars"] = data["Password"].apply(lambda p: set(p)).apply(len)
    return data
