import os
import pickle

import joblib
import numpy as np
import pandas as pd
import toml
from lightgbm import LGBMRegressor
from loguru import logger
from scipy.sparse import hstack
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

from lgbm_model.utils import add_features, process_password, rmsle_lgbm


class PasswordLGBM:
    """ LightGBM model to predict password frequency"""

    def __init__(self, model_serialized=None, vectorizer=None):
        """
        Initialize model configuration
        """
        self.config = toml.load("config.toml")
        self.data_dir = self.config["project"]["data_dir"]
        self.res_dir = self.config["project"]["resources_path"]

        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)

        self.model = None

        self.model = None
        if model_serialized:
            self.model = joblib.load(
                os.path.join(self.res_dir, model_serialized),
            )
        self.tokenizer = None
        if vectorizer:
            self.vectorizer = os.path.join(self.res_dir, vectorizer)

    def predict(self, password: str) -> float:
        """Predict password frequency
        Args:
            password: unique password
        Returns:
            frequency prediction

        """
        if password == "":
            return 0
        token = process_password(self.vectorizer, password)
        y_pred = self.model.predict(token)
        y_pred = np.exp(y_pred[0])
        return round(y_pred, 4)

    def _load_data(self, train_file: str, test_file: str) -> object:
        """Load and basic processing of train and test data

        Args:
            train_file: zip file for train data
            test_file: zip file for test data

        Returns:
            y: pass frequency
            full_df: train+test passes
            n_train_recs: count of train records
        """
        train_data = pd.read_csv(
            os.path.join(self.data_dir, train_file), compression="zip"
        )
        test_data = pd.read_csv(
            os.path.join(self.data_dir, test_file), compression="zip"
        )
        train_data.dropna(inplace=True)
        y = train_data["Times"]
        train_data.drop(columns="Times", inplace=True)
        test_data.drop(columns="Id", inplace=True)
        full_df = train_data.append(test_data)
        n_train_recs = train_data.shape[0]
        logger.info("data loading done")
        return np.log(y), full_df, n_train_recs

    @staticmethod
    def _process_pass_data(data: pd.DataFrame) -> pd.DataFrame:
        """Preprocessing for common data
        Args:
            data:
        Returns:
            dataframe with processed passes
            max_pass_len: max length for pass in train and test
            dict_len: count of symbols
        """
        # fill nan values
        data.fillna(" ", inplace=True)
        data = add_features(data)

        logger.info("processing data done")
        return data

    def _fit_vectorizer(self, data: pd.DataFrame):
        self.vectorizer = CountVectorizer(
            lowercase=False,
            analyzer="char",
            ngram_range=(1, 3),
            min_df=10,
            max_features=50000,
        )
        self.vectorizer.fit(data["Password"])
        logger.info("fitting vectorizer done...")
        return self.vectorizer.transform(data["Password"])

    def save_vectorizer(self, vectorizer_name: str):
        full_name = os.path.join(self.res_dir, vectorizer_name)
        with open(full_name, "wb") as handle:
            pickle.dump(self.vectorizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        logger.info("saving vectorizer done")
        return None

    @staticmethod
    def _split_data(x_data, y_data, test_size, n_train_recs, random_state):
        """
        Split data for train
        """
        x = x_data[:n_train_recs, :]  # train part
        x_test = x_data[n_train_recs:, :]  # test part
        x_train, x_val, y_train, y_val = train_test_split(
            x,
            y_data,
            test_size=test_size,
            train_size=1 - test_size,
            random_state=random_state,
        )
        logger.info("splitting data done")
        return x_train, x_val, y_train, y_val, x_test

    def fit(
        self,
        train_file: str,
        test_file: str,
        test_size=0.1,
        random_state=42,
    ):
        """
        Train model
        """
        y, full_df, n_train_recs = self._load_data(train_file, test_file)
        full_df = self._process_pass_data(full_df)
        data_vectorized = self._fit_vectorizer(full_df)
        data = hstack(
            [
                data_vectorized,
                full_df.drop(columns=["Password"], errors="ignore").values,
            ]
        )
        data = data.tocsr()
        x_train, x_val, y_train, y_val, _ = self._split_data(
            data,
            y,
            test_size=test_size,
            n_train_recs=n_train_recs,
            random_state=random_state,
        )

        model = LGBMRegressor(
            n_estimators=100,
            num_leaves=31,
            max_depth=-1,
            learning_rate=0.05,
            subsample=0.8,
            subsample_freq=0,
            colsample_bytree=0.8,
            reg_alpha=0.1,
            reg_lambda=0.01,
            random_state=42,
            n_jobs=-1,
        )

        logger.info("start training the model...")

        model.fit(
            x_train,
            y_train,
            early_stopping_rounds=5,
            eval_set=[(x_val, y_val)],
            eval_metric=rmsle_lgbm,
            verbose=1,
        )
        self.model = model

        logger.info("training model done")
        return None

    def save_model(self, model_name: str):
        """
        Save lgbm-model with model_name
        """
        # save the model
        joblib.dump(self.model, os.path.join(self.res_dir, model_name))
        logger.info("saving model done")
        return None
