from flask import Flask, request, render_template
from lgbm_model.model import PasswordLGBM

# Flask instance
app = Flask(__name__)


# Model class instance
password_model = PasswordLGBM(
    model_serialized="lgbm_model", vectorizer="vectorizer.pickle"
)


@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == "POST":
        pw = request.form["password"]
        pass_freq = password_model.predict(pw)
        return render_template("index.html", password=pw, prediction=pass_freq)
    else:
        return render_template("index.html")


if __name__ == "__main__":
    # For development set "debug=True"in app.run
    app.run(host="0.0.0.0", threaded=False, debug=True)
