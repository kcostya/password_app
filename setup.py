from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Password App ML Project",
    author="Kostyantyn Kravchenko",
    license="BSD-3",
)
