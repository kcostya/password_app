In order to launch `train.py`, please load the files from https://www.kaggle.com/c/dmia-sport-2019-fall-intro/data and put to `data/raw/` dir: 
- `data/raw/Xtest.csv.zip`
- `data/raw/train.csv.zip`
